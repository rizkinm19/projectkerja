<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'LoginController@index');


Route::get('/jabatan','JabatanController@index');
Route::get('/pegawai','PegawaiController@index');
Route::get('/user','UserController@index');
Route::get('/', 'LoginController@index');
Route::get('/home', 'homeController@index');
Route::get('/progres', 'ProgresKerjaController@index');
Route::get('/ubah', 'GantiPasswordController@index');