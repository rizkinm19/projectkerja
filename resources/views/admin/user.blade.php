<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Master | user</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">



  <!-- Navbar -->
  
  <!-- /.navbar -->
@include('admin/header')
  <!-- Main Sidebar Container -->
  @include('admin/sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page Pegawai) -->
    <div class="content-User">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">User</h1>

          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active">user</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="content">
      <div class="container-fluid">
          
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title"></h5>

                <p class="card-text">
                  
                </p>

     <section class="content">
      <div class="container-fluid">
      <div class="content">
      <div class="container-fluid">
        @yield('content')
      <div class="row">
      <div class="col-md-12">
      <div class="card">
      <div class="card-header">
      <h3 class="card-title"></h3>
                                  
                  <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">
                      <i class="fas fa-edit"> </i> Add </button>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">User</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
        </form>

                  <label>Minimal</label>
                  <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                    <option selected="selected" data-select2-id="31">Alabama</option>
                    <option data-select2-id="3">Alaska</option>
                    <option data-select2-id="32">California</option>
                    <option data-select2-id="33">Delaware</option>
                    <option data-select2-id="34">Tennessee</option>
                    <option data-select2-id="35">Texas</option>
                    <option data-select2-id="36">Washington</option>
                  </select>
            <span class="select2 select2-container select2-container--default select2-container--below select2-container--open" dir="ltr" data-select2-id="2" style="width: 100%;">
              <span class="selection">
                <span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="true" tabindex="0" aria-disabled="false" aria-labelledby="select2-mgxj-container" aria-owns="select2-mgxj-results" aria-activedescendant="select2-mgxj-result-i68g-Texas">
                  <span class="select2-selection__rendered" id="select2-mgxj-container" role="textbox" aria-readonly="true" title="Alaska">Alaska</span>
                    <span class="select2-selection__arrow" role="presentation">
                      <b role="presentation"></b>
                    </span>
                </span>
              </span>
            <span class="dropdown-wrapper" aria-hidden="true"></span>
              </span>
                <span class="select2-dropdown select2-dropdown--below" dir="ltr" style="width: 506.5px;">
                  <span class="select2-search select2-search--dropdown">
                    <input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" aria-controls="select2-mgxj-results" aria-activedescendant="select2-mgxj-result-i68g-Texas">
                  </span>
                    <span class="select2-results">
                      <ul class="select2-results__options" role="listbox" id="select2-mgxj-results" aria-expanded="true" aria-hidden="false">
                        <li class="select2-results__option" id="select2-mgxj-result-3a5b-Alabama" role="option" aria-selected="false" data-select2-id="select2-mgxj-result-3a5b-Alabama">Alabama</li>
                        <li class="select2-results__option" id="select2-mgxj-result-yv7r-Alaska" role="option" aria-selected="true" data-select2-id="select2-mgxj-result-yv7r-Alaska">Alaska</li>
                        <li class="select2-results__option" id="select2-mgxj-result-fjie-California" role="option" aria-selected="false" data-select2-id="select2-mgxj-result-fjie-California">California</li><li class="select2-results__option" id="select2-mgxj-result-nbny-Delaware" role="option" aria-selected="false" data-select2-id="select2-mgxj-result-nbny-Delaware">Delaware</li>
                        <li class="select2-results__option" id="select2-mgxj-result-jbqn-Tennessee" role="option" aria-selected="false" data-select2-id="select2-mgxj-result-jbqn-Tennessee">Tennessee</li>
                        <li class="select2-results__option select2-results__option--highlighted" id="select2-mgxj-result-i68g-Texas" role="option" aria-selected="false" data-select2-id="select2-mgxj-result-i68g-Texas">Texas</li>
                        <li class="select2-results__option" id="select2-mgxj-result-ktiv-Washington" role="option" aria-selected="false" data-select2-id="select2-mgxj-result-ktiv-Washington">Washington</li>
                      </ul>
                    </span>
                  </span>
                
        <div class="form-group">
            <label for="recipient-name" class="col-form-label">Password</label>
            <input type="password" class="form-control" id="recipient-name">
          </div>  
         <div class="form-group">
            <label for="recipient-name" class="col-form-label">Konfirmasi password</label>
            <input type="password" class="form-control" id="recipient-name">
          </div>   
        

      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
                    </i>
                  </button>
                  
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                    <div class="input-group-append">
                      <button type="button" class="btn bg-gradient-primary btn-sm">
                      <i class="fas fa-search">
                      </i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <table id="example2" class="table table-bordered table-hover dataTable" role="grid"aria-describedby="example2_info">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Pegawai</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   
                    <tr>
                      <td>1</td>
                      <td>1</td>
                      <td>1</td>
                      <td>1</td>
                      <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">
                      <i class="fas fa-edit"> </i> Edit </button>
                      <button type="button" class="btn  bg-danger"  data-target="#exampleModal" data-whatever="@getbootstrap">
                      <i class="fas fa-delete"> </i> Hapus </button>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Jabatan:</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Pegawai:</label>
            <textarea class="form-control" id="recipient-name"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div></td> 
                    
                    </tr>                 
                  </tbody>
                </table>
              </div>
              </div>
              <!button next n previous>
              <div class="row"><div class="col-sm-12 col-md-5">
                <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
              </div>
              <div class="col-sm-12 col-md-7">
                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                  <ul class="pagination">
                    <li class="paginate_button page-item previous disabled" id="example2_previous">
                      <a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0" class="page-link">Previous</a>
                    </li><li class="paginate_button page-item active"><a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                    <li class="paginate_button page-item "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0" class="page-link">6</a></li><li class="paginate_button page-item next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
            </div>
               </div>
    </div>
  </div>
</div>
            </div>
          <!-- col-sm-7 -->           
        </div>
        <!-- row --> 
      </div>
      <!-- pd-20 --> 
    </div>
                         
                          
                
    
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
</body>
</html>
