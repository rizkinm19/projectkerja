
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Progres Kerja </title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  
  <!-- /.navbar -->
 @include('admin/header')
  <!-- Main Sidebar Container -->
  @include('admin/sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Progres Kerja</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Progres Kerja</a></li>
              <li class="breadcrumb-item active">Progres Kerja</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
  <div class="content">
      <div class="container-fluid">          
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title"></h5>
              <p class="card-text">
            </p>
          <section class="content">
      <div class="container-fluid">
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        @yield('content')
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Progres Kerja</h3>                  
                  <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                 
                  <!-- /.Button plus add atas table -->              
                  <button style="margin-left:-50px" type="button" class="btn bg-gradient-primary btn-sm" data-toggle="modal" data-target="#ModalTambah" data-whatever="@getbootstrap">
              <i class="fas fa-plus">Add</i>
            </button>

  <!-- /.pop up plus -->
<div>
  <div class="modal fade" id="ModalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">ADD</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times; </span>
            </button>
        </div>

<!-- /.isi pop up  -->
 <script language="javascript">
  function addRow(tableID) {

   var table = document.getElementById(tableID);
   var rowCount = table.rows.length;
   var row = table.insertRow(rowCount);

   var cell1 = row.insertCell(0);
   var element1 = document.createElement("input");
   element1.type = "checkbox";
   element1.name="chkbox";
   cell1.appendChild(element1);

   var cell2 = row.insertCell(1);
   var element2 = document.createElement("input");
   element2.type = "date";
   element2.name = "tanggal";
   element2.class = "form-control";
   cell2.appendChild(element2);

   var cell3 = row.insertCell(2);
   var element3 = document.createElement("input");
   element3.type = "text";
   element3.name="tulisan";
   element3.class ="form-control";
   cell3.appendChild(element3);
  }

  function deleteRow(tableID) {
   try {
   var table = document.getElementById(tableID);
   var rowCount = table.rows.length;

   for(var i=0; i<rowCount; i++) {
    var row = table.rows[i];
    var chkbox = row.cells[0].childNodes[0];
    if(null != chkbox && true == chkbox.checked) {
     table.deleteRow(i);
     rowCount--;
     i--;
    }
   }
   }catch(e) {
    alert(e);
   }
  }
  // $('.remove').live('click',function(){
  //  var last=$('tbody tr').length;
  //      if(last==1){
  //          alert("you can not remove last row");
  //      }
  //      else{
  //           $(this).parent().parent().remove();
  //      }
  //  });
 </script>
</head>

<div>
    <div style="padding: 20px 0px 0px 0px;">      
        <table id="dataTable" style="border-bottom: 4px solid #0099FF; border-left: 4px solid #0099FF; border-right: 4px solid #0099FF; border-top: 4px solid #0099FF; width: 498px;">
          
          <br>
            <tr>
              <td><input name="chk" type="checkbox" /></td>
              <td><input name="tanggal" type="date" /></td>     
              <td><input name="tulisan" type="text" /></td>
            </tr>
            <tr>
              <td><input name="chk" type="checkbox" /></td>
              <td><input name="tanggal" type="date" /></td>
              <td><input name="tulisan" type="text" /></td>
            </tr>
          </table>
          <br>
          <center>
        <input onclick="addRow('dataTable')" type="button" class="btn btn-primary" value="Tambah" />
        <input onclick="deleteRow('dataTable')" type="button" class="btn btn-secondary" value="Hapus"/></center>
    </div>
</div>
<!-- /.membuat footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Add</button>
      </div>
      </div>
    </div>
  </div>
</div>
</button>

<!-- /.membuat search -->
                  <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search">
                        </i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <table id="example2" class="table table-bordered table-hover dataTable" role="grid"aria-describedby="example2_info">
                  <thead>
                    <tr> 
                      <th>NO</th>
                      <th>Tanggal</th>
                      <th>Pegawai</th>
                      <th>Kegiatan</th> 
                      <th>Action</th> 
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>10</td>
                      <td>11-7-2019</td>
                      <td>Sukijan</td>
                      <td>Maintenance</td> 
                      <td>
<center>
<!-- /.membuat checkbox -->
<input style="margin-right:-100px" type="checkbox" class="btn bg-gradient-primary btn-lg" >
<!-- /.membuat tombol Edit -->
              <button style="margin-right:-100px" type="button" class="btn bg-gradient-primary btn-sm" data-toggle="modal" data-target="#modalEdit" data-whatever="@getbootstrap">
              <i class="fas fa-edit"></i> Edit</button>

<!-- /.membuat pop up edit -->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">EDIT</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group" >
            <div class="col-md-10">
            <label for="recipient-name" class="col-form-label">NO:</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
        </div>
          <div class="form-group" >
            <div class="col-md-10">
            <label for="recipient-name" class="col-form-label">Tanggal:</label>
            <input type="date" class="form-control" id="recipient-name">
          </div>
        </div>
         <div class="form-group" >
            <div class="col-md-10">
            <label for="recipient-name" class="col-form-label">Kegiatan:</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
                  </button>
                </center>
              </td>
            </tr>                  
          </tbody>
        </table>  <br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
                <!-- /.show and page -->
              <div class="row">
          <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries
            </div>
          </div>
              <div class="col-sm-12 col-md-7">
                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                  <ul class="pagination">
                    <li class="paginate_button page-item previous disabled" id="example2_previous">
                      <a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0" class="page-link">Previous</a>
                    </li>
                    <li class="paginate_button page-item active">
                      <a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0" class="page-link">1
                      </a>
                    </li>
                    <li class="paginate_button page-item ">
                      <a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0" class="page-link">2
                      </a>
                    </li>
                    <li class="paginate_button page-item ">
                      <a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0" class="page-link">3
                      </a>
                    </li>
                    <li class="paginate_button page-item ">
                      <a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0" class="page-link">4
                      </a>
                    </li>
                    <li class="paginate_button page-item ">
                      <a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0" class="page-link">5
                      </a>
                    </li>
                    <li class="paginate_button page-item ">
                      <a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0" class="page-link">6
                      </a>
                    </li>
                    <li class="paginate_button page-item next" id="example2_next">
                      <a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0" class="page-link">Next
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          </div>
          </div>
          </div>
              <!-- /.card-body -->

            <!-- /.card -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('admin/footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
</body>
</html>